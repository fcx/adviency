import { TestBed } from '@angular/core/testing';

import { RegalosServiceService } from './regalos-service.service';

describe('RegalosServiceService', () => {
  let service: RegalosServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegalosServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
