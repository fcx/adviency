import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { gifties, regalo } from '../components/main/regalos';

@Injectable({
  providedIn: 'root'
})
export class RegalosServiceService {

  regalos: regalo[] = [];


  constructor() {

    const storedRegalos = JSON.parse(localStorage.getItem('regalos')!);


    this.regalos = storedRegalos || [];
  }

  getRegalos(): Observable<regalo[]> {
    return of(this.regalos);
  }

  addRegalo(regalo: regalo): void {
    const existingValue = this.regalos.find(r => r.name === regalo.name);
    if (existingValue) {
      alert("Ya esta anotado ese regalo!");
      return;
    }

    this.regalos.push(regalo);
    localStorage.setItem('regalos', JSON.stringify(this.regalos));
  }

  removeRegalo(regalo: regalo): void {
    this.regalos = this.regalos.filter(item => item !== regalo);
    localStorage.setItem('regalos', JSON.stringify(this.regalos));
  }

  removeAll(): void {
    this.regalos.splice(0, this.regalos.length);
    localStorage.setItem('regalos', JSON.stringify(this.regalos));
  }

  updateRegalo(regalo: regalo): void {
    // Find the index of the regalo to be updated
    const index = this.regalos.findIndex(r => r.id === regalo.id);

    // Update the regalo in the array
    this.regalos = this.regalos.map((r, i) => {
      if (i === index) {
        return regalo;
      }
      return r;
    });

    localStorage.setItem('regalos', JSON.stringify(this.regalos));
  }

  duplicateRegalo(regalo: regalo): void {
    // const existingValue = this.regalos.find(r => r.name === regalo.name);
    // const existingName = this.regalos.find(r => r.user === regalo.user);
    // if (existingName) {
    //   alert("Ya esta anotado ese regalo para ese destino!");
    //   return;
    // }

    this.regalos.push(regalo);
    localStorage.setItem('regalos', JSON.stringify(this.regalos));
  }

  isGiftDuplicate(name: string, user: string): boolean {
    const existingName = this.regalos.find(r => r.name === name && r.user === user);
    return !!existingName;
  }

}
