import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { regalo, gifties } from './regalos';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { defaultIfEmpty, Observable, of } from 'rxjs';
import { RegalosServiceService } from 'src/app/services/regalos-service.service';



@Component({
  selector: 'app-main',
  standalone: true,
  imports: [CommonModule, MainComponent, ReactiveFormsModule, FormsModule],
  templateUrl: './main.component.html',

})
export class MainComponent implements OnInit {


  regalos!: Observable<regalo[]>;
  showModal = false;
  showModalEditar = false;
  showModalDuplicar = false;
  showModalLista = false;
  regaloForm!: FormGroup;
  precioTotalRegalos = 0;
  @ViewChild('song') song!: ElementRef<HTMLAudioElement>;
  regalosRandom = ['Play 5', 'Xbox', 'Mesa de Ping Pong', 'Soporte Monitores', 'Banco Press', 'Teclado', 'Pc upgrade', 'SmartWatch', 'Silla Erasmos', 'Celular'];



  constructor(private formBuilder: FormBuilder, private regalosService: RegalosServiceService) { }

  ngOnInit(): void {

    this.regalos = this.regalosService.getRegalos();



    this.regaloForm = new FormGroup({
      name: new FormControl('', Validators.required),
      user: new FormControl('', Validators.required),
      img: new FormControl('', Validators.required),
      cantidad: new FormControl(),
      precio: new FormControl()
    });


    //updateo el precio total de los regalos
    this.calculateTotalPrice();
    this.regaloForm.reset();
  }


  onAddRegalo() {

    const value = this.regaloForm.value.name;
    let cantidad = this.regaloForm.value.cantidad;
    const user = this.regaloForm.value.user;
    const img = this.regaloForm.value.img;
    const precio = this.regaloForm.value.precio;

    this.regaloForm.reset();


    let nextId = 1;
    this.regalos.subscribe(regalos => {
      if (regalos.length > 0) {
        // Find the highest ID in the array
        const highestId = regalos.reduce<number>((prev, curr) => {
          return Math.max(prev, curr.id);
        }, 0);

        // Set the next ID to be one higher than the highest ID
        if (highestId > 0) {
          nextId = highestId + 1;
        }
      }
    });
    //si la cantidad es null , le pongo 1 si no , devuelvo la cantidad ingresada en el input
    cantidad === null ? cantidad = 1 : cantidad;




    this.regalosService.addRegalo({ id: nextId, name: `${value}`, cantidad: cantidad, user: user!, img: img!, precio: precio });

    //updateo el precio total de los regalos
    this.calculateTotalPrice();

    //reseteo la el form para que no ea repetido el input
    this.regaloForm.reset();
    this.regalos = this.regalosService.getRegalos();
    this.showModal = false;
  }

  onRemoveRegalo(regalo: regalo): void {
    this.regalosService.removeRegalo(regalo);
    this.regalos = this.regalosService.getRegalos();
    //updateo el precio total de los regalos
    this.calculateTotalPrice();
  }
  onRemoveTodos(): void {
    this.regalosService.removeAll();
    this.regalos = this.regalosService.getRegalos();
    //updateo el precio total de los regalos
    this.calculateTotalPrice();
  }


  onEditRegalo(regalo: any): void {
    this.regaloForm = this.formBuilder.group({
      id: [regalo.id],
      name: [regalo.name],
      precio: [regalo.precio],
      user: [regalo.user],
      img: [regalo.img],
      cantidad: [regalo.cantidad],

    });


    //updateo la lista y la displayeo
    this.regalosService.updateRegalo(regalo);


    //updateo la lista de regalos
    this.regalos = this.regalosService.getRegalos();

    //updateo el precio total de los regalos
    this.calculateTotalPrice();

    //cierro el modal para editar
    this.showModalEditar = false;
  }

  onDuplicateRegalo(regalo: any): void {

    let nuevaId = Math.floor(Math.random() * 100) + 1;
    this.regaloForm = this.formBuilder.group({
      id: [nuevaId],
      name: [regalo.name],
      precio: [regalo.precio],
      user: [""],
      img: [regalo.img],
      cantidad: [regalo.cantidad],

    });

    // Check if the combination of regalo.name and regalo.user is already in the list of gifts
    if (this.regalosService.isGiftDuplicate(regalo.name, regalo.user)) {
      alert("Estas duplicando el regalo , Asegurate de cambiar Para quien 😋");
      return;
    }


    //updateo la lista y la displayeo
    this.regalosService.duplicateRegalo(regalo);


    //updateo la lista de regalos
    this.regalos = this.regalosService.getRegalos();

    //updateo el precio total de los regalos
    this.calculateTotalPrice();

    //cierro el modal para editar
    this.showModalDuplicar = false;
  }

  givemeRandomGift(): string {
    const index = Math.floor(Math.random() * this.regalosRandom.length);

    //conseguir el valor del formulario y setearlo al input
    const nameControl = this.regaloForm.get('name');
    nameControl!.setValue(this.regalosRandom[index]);

    // marcar el name input para marcar como que ya fue triggereado el name input
    nameControl!.markAsTouched();

    //updateo la lista de regalos
    this.regalos = this.regalosService.getRegalos();

    return this.regalosRandom[index];
  }
  calculateTotalPrice() {
    this.precioTotalRegalos = 0;
    this.regalos.subscribe(regalos => {
      regalos.forEach(regalo => {
        this.precioTotalRegalos += regalo.precio * regalo.cantidad;
      });
    });
  }

  printPantalla(): void {
    window.print();
  }

  playSong(): void {
    // If the audio is currently paused, play it; otherwise, pause it
    console.log("ESTOY ESCUCHANDO MUSIK");
    if (this.song.nativeElement.paused) {
      this.song.nativeElement.play();
    } else {
      this.song.nativeElement.pause();
    }
  }
}



